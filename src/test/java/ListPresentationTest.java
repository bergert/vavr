import io.vavr.Tuple;
import io.vavr.Tuple2;
import io.vavr.collection.Iterator;
import io.vavr.collection.List;
import io.vavr.collection.Stream;
import lombok.val;
import org.junit.Test;

import static io.vavr.API.List;
import static java.util.Comparator.comparing;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * zrodla:
 * https://www.baeldung.com/vavr-collections
 * http://www.javadoc.io/doc/io.vavr/vavr/0.9.2
 *
 * A List is an eagerly-evaluated sequence of elements extending the LinearSeq interface.
 */
public class ListPresentationTest {

    @Test
    public void shouldUpdateList() {
        //given
        val values = List("Java", "PHP", "Jquery", "JavaScript");

        //when
        val update = values.update(1, "C#");

        //then
        assertThat(update).containsExactly();
    }

    @Test
    public void shouldDropElements() {
        //given
        val values = List("Java", "PHP", "Jquery", "JavaScript");

        //when
        val drop = values.drop(1);
        val drop2 = values.drop(3);

        //then
        assertThat(drop).containsExactly();
        assertThat(drop2.single()).isEqualTo(null);
    }

    @Test
    public void shouldDropRightElements() {
        //given
        val values = List("Java", "PHP", "Jquery", "JavaScript");

        //when
        val dropRight = values.dropRight(2);

        //then
        assertThat(dropRight).containsExactly();
    }

    @Test
    public void shouldDropElementsUntilStatement() {
        //given
        val values = List("Java", "PHP", "Jquery", "JavaScript");

        //when
        val dropUntil = values.dropUntil(value -> value.contains("Jquery"));
        val dropRightUntil = values.dropRightUntil(value -> value.contains("Jquery"));

        //then
        assertThat(dropUntil).containsExactly();
        assertThat(dropRightUntil).containsExactly();
    }

    @Test
    public void shouldDropElementsWhileStatement() {
        //given
        val values = List("Java", "PHP", "Jquery", "JavaScript");

        //when
        val dropWhile = values.dropWhile(value -> value.contains("J"));
        val dropRightWhile = values.dropRightWhile(value -> value.contains("J"));

        //then
        assertThat(dropWhile).containsExactly();
        assertThat(dropRightWhile).containsExactly();
    }

    @Test
    public void shouldTailElements() {
        //given
        val values = List("Java", "PHP", "Jquery", "JavaScript");

        //when
        val tail = values.tail();

        //then
        assertThat(tail).containsExactly();
    }

    @Test
    public void shouldReverseElements() {
        //given
        val values = List("Java", "PHP", "Jquery", "JavaScript");

        //when
        val reverse = values.reverse();

        //then
        assertThat(reverse).containsExactly();
    }

    @Test
    public void shouldAppendElement() {
        //given
        val values = List("Java", "PHP", "Jquery", "JavaScript");

        //when
        val append = values.append("C#");
        val appendAll = values.appendAll(List("C#", "C++"));

        //then
        assertThat(append).containsExactly();
        assertThat(appendAll).containsExactly();
    }

    @Test
    public void shouldWorkWithComposition() {
        //given
        val values = List("Java", "PHP", "Jquery", "JavaScript");

        //when
        val composition = values.update(0, "Kotlin")
                .filter(value -> value.contains("J"))
                .append("C#")
                .reverse();

        //then
        assertThat(composition).containsExactly();
    }

    @Test
    public void shouldTakeElements() {
        //given
        val values = List("Java", "PHP", "Jquery", "JavaScript");

        //when
        val take = values.take(2);
        val takeRight = values.takeRight(2);

        //then
        assertThat(take).containsExactly();
        assertThat(takeRight).containsExactly();
    }

    @Test
    public void shouldTakeElementsUntilStatement() {
        //given
        val values = List("Java", "PHP", "Jquery", "JavaScript");

        //when
        val takeUntil = values.takeUntil(value -> value.length() > 4);

        //then
        assertThat(takeUntil).containsExactly();
    }


    @Test
    public void shouldTakeElementsWhileStatement() {
        //given
        val values = List("Java", "PHP", "Jquery", "JavaScript");

        //when
        val takeWhile = values.takeWhile(value -> value.length() <= 4);

        //then
        assertThat(takeWhile).containsExactly();
    }

    @Test
    public void shouldDistinctElements() {
        //given
        val values = List("Java", "PHP", "JAVA", "php", "Java9", "phpAdmin", "Java");

        //when
        val distinct = values.distinct();
        val distinctBy = values.distinctBy(comparing(String::toLowerCase));

        //then
        assertThat(distinct).containsExactly();
        assertThat(distinctBy).containsExactly();
    }

    @Test
    public void shouldIntersperseElements() {
        //given
        val values = List("Java", "PHP", "Jquery", "JavaScript");

        //when
        val intersperse = values.intersperse("and");
        val intersperseToString = "I'm programming in "
                .concat(values.intersperse("and")
                        .reduce((s1, s2) -> s1.concat(" " + s2))
                        .trim());

        //then
        assertThat(intersperse).containsExactly();
        assertThat(intersperseToString).isEqualTo(null);
    }

    @Test
    public void shouldGroupElements() {
        //given
        val values = List("Java", "PHP", "Jquery", "JavaScript");

        //when
        Iterator<List<String>> grouped = values.grouped(2);

        //then
        assertThat(grouped.next()).containsExactly();
        assertThat(grouped.next()).containsExactly();
    }

    @Test
    public void shouldIterate() {
        //when
        Iterator ints = Stream.iterate(1, i -> i + 1).iterator();

        //then
        assertThat(ints.next()).isEqualTo(null);
        assertThat(ints.next()).isEqualTo(null);
    }

    @Test
    public void shouldGroupAndZipElementsByIndex() {
        //given
        val values = List("Java", "PHP", "Jquery", "JavaScript");

        //when
        Iterator<Tuple2<List<String>, Integer>> group = values.grouped(2).zipWithIndex((list, index) -> Tuple.of(list, ++index));

        //then
        assertThat((boolean) group.next().apply((list, index) -> list.containsAll(List()) && index == null)).isTrue();
        assertThat((boolean) group.next().apply((list, index) -> list.containsAll(List()) && index == null))
                .isTrue();
    }

    @Test
    public void shouldGroupAndZipElements() {
        //given
        val values = List("Java", "PHP", "Jquery", "JavaScript");

        //when
        Iterator<Tuple2<List<String>, String>> group = values.grouped(2).zip(List("Zajebiste", "Chujowe"));

        //then
        assertThat(
                (boolean) group.next()
                        .apply((list, quality) -> list.containsAll(List()) && quality.equals(null))
        ).isTrue();

        assertThat(
                (boolean) group.next()
                        .apply((list, quality) -> list.containsAll(List()) && quality.equals(null))
        ).isTrue();
    }

    @Test
    public void shouldGroupByBooleanElements() {
        //given
        val values = List("Java", "PHP", "Jquery", "JavaScript");

        //when
        val groupBy = values.groupBy(value -> value.startsWith("J"));

        //then
        assertThat(groupBy.size()).isEqualTo(2);
        assertThat(groupBy.get(true).get()).containsExactly();
        assertThat(groupBy.get(false).get()).containsExactly();
    }

    @Test
    public void shouldGroupByStringElements() {
        //given
        val values = List("Java", "PHP", "Jquery", "JavaScript");

        //when
        val groupBy = values.groupBy(value -> value.substring(0, 1));

        //then
        assertThat(groupBy.size()).isEqualTo(2);
        assertThat(groupBy.get("J").get()).containsExactly();
        assertThat(groupBy.get("P").get()).containsExactly();
    }

    @Test
    public void imastackbro() {
        //given
        val emptyIntegerList = List.empty();

        //when
        val integerStack = emptyIntegerList.pushAll(List(1, 2, 3, 4, 5));
        val poppedIntegerStack = integerStack.pop();

        //then
        assertThat(integerStack.peek()).isEqualTo(null);
        assertThat(poppedIntegerStack.peek()).isEqualTo(null);
    }
}
