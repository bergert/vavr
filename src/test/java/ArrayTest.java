import io.vavr.collection.Array;
import lombok.val;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

//An Array is an immutable, indexed, sequence that allows efficient random access. It is backed by a Java array of objects.
public class ArrayTest {

    @Test
    public void shouldReturnElementsByRange() {
        //when
        Array<Integer> range = Array.range(1, 5);

        //then
        assertThat(range).containsExactly(1, 2, 3, 4);
    }

    @Test
    public void shouldReturnElementsByClosedRange() {
        //when
        val closedRange = Array.rangeClosed(1, 5);

        //then
        assertThat(closedRange).containsExactly(1, 2, 3, 4, 5);
    }

    @Test
    public void shouldRemoveAtIndex() {
        //given
        val array = Array.of(1, 2, 3, 4, 5);

        //when
        val remove = array.remove(1);
        val removeAt = array.removeAt(1);
        val removeAll = array.removeAll(i -> i % 2 == 0);

        //then
        assertThat(remove).containsExactly(2, 3, 4, 5);
        assertThat(removeAt).containsExactly(1, 3, 4, 5);
        assertThat(removeAll).containsExactly(1, 3, 5);
    }

    @Test
    public void shouldReplace() {
        //given
        val array = Array.tabulate(5, i -> i + 1);

        //when
        val replace = array.replace(1, 5);

        //then
        assertThat(array).containsExactly(1, 2, 3, 4, 5);
        assertThat(replace).containsExactly(5, 2, 3, 4, 5);
    }
}
