import io.vavr.Tuple;
import io.vavr.Tuple2;
import io.vavr.collection.Iterator;
import io.vavr.collection.List;
import io.vavr.collection.Stream;
import lombok.val;
import org.junit.Test;

import static io.vavr.API.List;
import static java.util.Comparator.comparing;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * zrodla:
 * https://www.baeldung.com/vavr-collections
 * http://www.javadoc.io/doc/io.vavr/vavr/0.9.2
 *
 * Vavr’s List is an immutable linked list. Mutations create new instances.
 * Most operations are performed in linear time. Consequent operations are executed one by one.
 */
public class ListTest {

    /*private List<String> prepareTestData() {
        return List("Java", "PHP", "Jquery", "JavaScript");
    }*/

    @Test
    public void shouldUpdateList() {
        //given
        val values = List("Java", "PHP", "Jquery", "JavaScript");

        //when
        val update = values.update(1, "C#");

        //then
        assertThat(update).containsExactly("Java", "C#", "Jquery", "JavaScript");
    }

    @Test
    public void shouldDropElements() {
        //given
        val values = List("Java", "PHP", "Jquery", "JavaScript");

        //when
        val drop = values.drop(1);
        val drop2 = values.drop(3);

        //then
        assertThat(drop).containsExactly("PHP", "Jquery", "JavaScript");
        assertThat(drop2.single()).isEqualTo("JavaScript");
    }

    @Test
    public void shouldDropRightElements() {
        //given
        val values = List("Java", "PHP", "Jquery", "JavaScript");

        //when
        val dropRight = values.dropRight(2);

        //then
        assertThat(dropRight).containsExactly("Java", "PHP");
    }

    @Test
    public void shouldDropElementsUntilStatement() {
        //given
        val values = List("Java", "PHP", "Jquery", "JavaScript");

        //when
        val dropUntil = values.dropUntil(value -> value.contains("Jquery"));
        val dropRightUntil = values.dropRightUntil(value -> value.contains("Jquery"));

        //then
        assertThat(dropUntil).containsExactly("Jquery", "JavaScript");
        assertThat(dropRightUntil).containsExactly("Java", "PHP", "Jquery");
    }

    @Test
    public void shouldDropElementsWhileStatement() {
        //given
        val values = List("Java", "PHP", "Jquery", "JavaScript");

        //when
        val dropWhile = values.dropWhile(value -> value.contains("J"));
        val dropRightWhile = values.dropRightWhile(value -> value.contains("J"));

        //then
        assertThat(dropWhile).containsExactly("PHP", "Jquery", "JavaScript");
        assertThat(dropRightWhile).containsExactly("Java", "PHP");
    }

    @Test
    public void shouldTailElements() {
        //given
        val values = List("Java", "PHP", "Jquery", "JavaScript");

        //when
        val tail = values.tail();

        //then
        assertThat(tail).containsExactly("PHP", "Jquery", "JavaScript");
    }

    @Test
    public void shouldReverseElements() {
        //given
        val values = List("Java", "PHP", "Jquery", "JavaScript");

        //when
        val reverse = values.reverse();

        //then
        assertThat(reverse).containsExactly("JavaScript", "Jquery", "PHP", "Java");
    }

    @Test
    public void shouldAppendElement() {
        //given
        val values = List("Java", "PHP", "Jquery", "JavaScript");

        //when
        val append = values.append("C#");
        val appendAll = values.appendAll(List("C#", "C++"));

        //then
        assertThat(append).containsExactly("Java", "PHP", "Jquery", "JavaScript", "C#");
        assertThat(appendAll).containsExactly("Java", "PHP", "Jquery", "JavaScript", "C#", "C++");
    }

    @Test
    public void shouldWorkWithComposition() {
        //given
        val values = List("Java", "PHP", "Jquery", "JavaScript");

        //when
        val composition = values.update(0, "Kotlin")
                .filter(value -> value.contains("J"))
                .append("C#")
                .reverse();

        //then
        assertThat(composition).containsExactly("C#", "JavaScript", "Jquery");
    }

    @Test
    public void shouldTakeElements() {
        //given
        val values = List("Java", "PHP", "Jquery", "JavaScript");

        //when
        val take = values.take(2);
        val takeRight = values.takeRight(2);

        //then
        assertThat(take).containsExactly("Java", "PHP");
        assertThat(takeRight).containsExactly("Jquery", "JavaScript");
    }

    @Test
    public void shouldTakeElementsUntilStatement() {
        //given
        val values = List("Java", "PHP", "Jquery", "JavaScript");

        //when
        val takeUntil = values.takeUntil(value -> value.length() > 4);

        //then
        assertThat(takeUntil).containsExactly("Java", "PHP");
    }


    @Test
    public void shouldTakeElementsWhileStatement() {
        //given
        val values = List("Java", "PHP", "Jquery", "JavaScript");

        //when
        val takeWhile = values.takeWhile(value -> value.length() <= 4);

        //then
        assertThat(takeWhile).containsExactly("Java", "PHP");
    }

    @Test
    public void shouldDistinctElements() {
        //given
        val values = List("Java", "PHP", "JAVA", "php", "Java9", "phpAdmin", "Java");

        //when
        val distinct = values.distinct();
        val distinctBy = values.distinctBy(comparing(String::toLowerCase));

        //then
        assertThat(distinct).containsExactly("Java", "PHP", "JAVA", "php", "Java9", "phpAdmin");
        assertThat(distinctBy).containsExactly("Java", "PHP", "Java9", "phpAdmin");
    }

    @Test
    public void shouldIntersperseElements() {
        //given
        val values = List("Java", "PHP", "Jquery", "JavaScript");

        //when
        val intersperse = values.intersperse("and");
        val intersperseToString = "I'm programming in "
                .concat(values.intersperse("and")
                        .reduce((s1, s2) -> s1.concat(" " + s2))
                        .trim());

        //then
        assertThat(intersperse).containsExactly("Java", "and", "PHP", "and", "Jquery", "and", "JavaScript");
        assertThat(intersperseToString).isEqualTo("I'm programming in Java and PHP and Jquery and JavaScript");
    }

    @Test
    public void shouldGroupElements() {
        //given
        val values = List("Java", "PHP", "Jquery", "JavaScript");

        //when
        Iterator<List<String>> grouped = values.grouped(2);

        //then
        assertThat(grouped.next()).containsExactly("Java", "PHP");
        assertThat(grouped.next()).containsExactly("Jquery", "JavaScript");
    }

    @Test
    public void shouldIterate() {
        //when
        Iterator ints = Stream.iterate(1, i -> i + 1).iterator();

        //then
        assertThat(ints.next()).isEqualTo(1);
        assertThat(ints.next()).isEqualTo(2);
    }

    @Test
    public void shouldGroupAndZipElementsByIndex() {
        //given
        val values = List("Java", "PHP", "Jquery", "JavaScript");

        //when
        Iterator<Tuple2<List<String>, Integer>> group = values.grouped(2).zipWithIndex((list, index) -> Tuple.of(list, ++index));

        //then
        assertThat((boolean) group.next().apply((list, index) -> list.containsAll(List("Java", "PHP")) && index == 1)).isTrue();
        assertThat((boolean) group.next().apply((list, index) -> list.containsAll(List("Jquery", "JavaScript")) && index == 2))
                .isTrue();
    }

    @Test
    public void shouldGroupAndZipElements() {
        //given
        val values = List("Java", "PHP", "Jquery", "JavaScript");

        //when
        Iterator<Tuple2<List<String>, String>> group = values.grouped(2).zip(List("Zajebiste", "Chujowe"));

        //then
        assertThat(
                (boolean) group.next()
                        .apply((list, quality) -> list.containsAll(List("Java", "PHP")) && quality.equals("Zajebiste"))
        ).isTrue();

        assertThat(
                (boolean) group.next()
                        .apply((list, quality) -> list.containsAll(List("Jquery", "JavaScript")) && quality.equals("Chujowe"))
        ).isTrue();
    }

    @Test
    public void shouldGroupByBooleanElements() {
        //given
        val values = List("Java", "PHP", "Jquery", "JavaScript");

        //when
        val groupBy = values.groupBy(value -> value.startsWith("J"));

        //then
        assertThat(groupBy.size()).isEqualTo(2);
        assertThat(groupBy.get(true).get()).containsExactly("Java", "Jquery", "JavaScript");
        assertThat(groupBy.get(false).get()).containsExactly("PHP");
    }

    @Test
    public void shouldGroupByStringElements() {
        //given
        val values = List("Java", "PHP", "Jquery", "JavaScript");

        //when
        val groupBy = values.groupBy(value -> value.substring(0, 1));

        //then
        assertThat(groupBy.size()).isEqualTo(2);
        assertThat(groupBy.get("J").get()).containsExactly("Java", "Jquery", "JavaScript");
        assertThat(groupBy.get("P").get()).containsExactly("PHP");
    }

    @Test
    public void imastackbro() {
        //given
        val emptyIntegerList = List.empty();

        //when
        val integerStack = emptyIntegerList.pushAll(List(1, 2, 3, 4, 5));
        val poppedIntegerStack = integerStack.pop();

        //then
        assertThat(integerStack.peek()).isEqualTo(5);
        assertThat(poppedIntegerStack.peek()).isEqualTo(4);
    }


}
