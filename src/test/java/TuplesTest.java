import io.vavr.Tuple;
import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * zrodlo: https://www.youtube.com/watch?v=2JTlFAjhL3U
 */
public class TuplesTest {

    @Test
    public void shouldApply() {
        //given
        val java8 = Tuple.of("java", 8);

        //when
        String result = java8.apply((string, integer) -> string + integer);

        //then
        assertThat(result).isEqualTo("java8");
    }

    @Test
    public void shouldMap() {
        //given
        val first = Tuple.of("java", 8);
        val second = first.map1(StringUtils::capitalize)
                .map2(i -> i + 1);

        //when
        String firstResult = first.apply((string, integer) -> string + integer);
        String secondResult = second.apply((string, integer) -> string + integer);

        //then
        assertThat(firstResult).isEqualTo("java8");
        assertThat(secondResult).isEqualTo("Java9");
    }

    @Test
    public void shouldUpdate() {
        //given
        val user = Tuple.of("Grzesiek", "grzesiek@gmail.com");

        //when
        String result = user.update2("grzesiek@comarch.com").apply((login, email) -> login + " " + email);

        //then
        assertThat(result).isEqualTo("Grzesiek grzesiek@comarch.com");
        assertThat((boolean) user.apply((login, email) -> login.equals("Grzesiek") && email.equals("grzesiek@gmail.com"))).isTrue();
    }
}