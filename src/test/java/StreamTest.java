import io.vavr.collection.Stream;
import lombok.val;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * zrodla:
 * https://www.baeldung.com/vavr-collections
 * http://www.javadoc.io/doc/io.vavr/vavr/0.9.2
 *
 * A Stream is an implementation of a lazy linked list and is quite different from java.util.stream.
 * Unlike java.util.stream, the Vavr Stream stores data and is lazily evaluating next elements.
 */
public class StreamTest {

    @Test
    public void shouldKeepOnlyHeadOfAStream() {
        //given
        Stream<Integer> stream = Stream.of(1, 2, 3, 4);
        Stream<Integer> streamTake = Stream.of(1, 2, 3, 4).take(2);
        Stream<Integer> infiniteStream = Stream.iterate(1, i -> i + 1);

        //when
        String streamString = stream.toString();
        String streamTakeString = streamTake.toString();
        String infiniteStreamString = infiniteStream.toString();

        //then
        assertThat(streamString).isEqualTo("Stream(1, ?)");
        assertThat(streamTakeString).isEqualTo("Stream(1, ?)");
        assertThat(infiniteStreamString).isEqualTo("Stream(1, ?)");
    }

    @Test
    public void shouldEvaluateElements() {
        //given
        Stream<Integer> infiniteStream = Stream.iterate(1, i -> i + 1);

        //when
        val value = infiniteStream.get(5);
        val sum = infiniteStream.take(10)
                .sum()
                .intValue();

        //then
        assertThat(value).isEqualTo(6);
        assertThat(sum).isEqualTo(55);
    }
}
