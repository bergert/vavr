import io.vavr.collection.List;
import io.vavr.control.Either;
import lombok.val;
import org.junit.Test;


public class EitherTest {

    private List<Account> prepareTestData() {
        return List.of(new Account("1", "1111111", 10.0),
                new Account("2", "22222222", 20.0));
    }

    private Either<Account, ErrorResponse> getAccountDetails(final String id) {
        val accountOptional = prepareTestData()
                .filter(account -> account.getId().equals(id))
                .peekOption();

        if (!accountOptional.isEmpty()) {
            return Either.left(accountOptional.get());
        } else {
            return Either.right(ErrorResponse.builder()
                    .code("13123")
                    .description("Rachunek o podanym id nie zostal odnaleziony")
                    .build());
        }
    }

    @Test
    public void shouldReturnPositiveResult() {

        //when


        //then
    }
}
